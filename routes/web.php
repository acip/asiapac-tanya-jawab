<?php

/** @var Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

use Laravel\Lumen\Routing\Router;

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['prefix' => '/api/v1'], function ($router) {
    // Question
    $router->group(['prefix' => '/question'], function ($router) {
        $router->get('/', 'QuestionController@index');
        $router->get('/{id}', 'QuestionController@show');
        $router->post('/', 'QuestionController@create');
        $router->put('/{id}', 'QuestionController@update');
        $router->delete('/{id}', 'QuestionController@delete');
    });

    // Question Option
    $router->group(['prefix' => '/question-option'], function ($router) {
        $router->get('/{id}', 'QuestionOptionController@show');
        $router->post('/', 'QuestionOptionController@create');
        $router->put('/{id}', 'QuestionOptionController@update');
        $router->delete('/{id}', 'QuestionOptionController@delete');
    });

    // Answer
    $router->group(['prefix' => '/answer'], function ($router) {
        $router->get('/', 'AnswerController@index');
        $router->post('/{questionId}', 'AnswerController@create');
    });
});
