<?php
namespace App\Repositories;

use App\Interfaces\QuestionOptionInterface;
use App\Models\Question;
use App\Models\QuestionOption;
use Exception;
use RuntimeException;

class QuestionOptionRepository implements QuestionOptionInterface {

    /**
     * @param $data
     * @return mixed
     */
    public function create($data)
    {
        if (!Question::find($data['question_id'])) {
            throw new RuntimeException('Question not Found');
        }

        $created = QuestionOption::create($data);
        $question= Question::with('options')->find($data['question_id']);

        // redis
        (new QuestionRepository())->setCached($question->id, $question);
        (new QuestionRepository())->refreshCachedQuestion();

        return QuestionOption::find($created->id);
    }

    /**
     * @param $id
     * @param $data
     * @return mixed
     */
    public function update($id, $data)
    {
        QuestionOption::where('id', $id)->update($data);
        $option = QuestionOption::find($id);

        // redis
        $question = Question::with('options')->find($option->question_id);
        if (!$question) {
            throw new RuntimeException('Question not Found');
        }
        (new QuestionRepository())->setCached($question->id, $question);
        (new QuestionRepository())->refreshCachedQuestion();

        return $option;
    }

    /**
     * @param $id
     * @return void
     * @throws Exception
     */
    public function delete($id)
    {
        $option = QuestionOption::find($id);
        if (!$option) {
            throw new RuntimeException('Question option not found');
        }
        QuestionOption::where('id', $id)->delete();

        // redis
        $question = Question::with('options')->find($option->question_id);
        (new QuestionRepository())->setCached($question->id, $question);
        (new QuestionRepository())->refreshCachedQuestion();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function get($id)
    {
        return QuestionOption::find($id);
    }
}