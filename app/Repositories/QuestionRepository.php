<?php
namespace App\Repositories;

use App\Interfaces\QuestionInterface;
use App\Models\Question;
use Exception;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Cache;
use Psr\SimpleCache\InvalidArgumentException;
use RuntimeException;

class QuestionRepository implements QuestionInterface {

    /**
     * @var string $store
     */
    private $store = 'redis';

    /**
     * @var string $index
     */
    private $index = 'question';

    /**
     * @param $id
     * @param $data
     * @return void
     */
    public function setCached($id, $data)
    {
        Cache::store($this->store)->put($this->index . '_' . $id, $data);
    }

    /**
     * @param $id
     * @return mixed
     * @throws InvalidArgumentException
     */
    public function getCached($id)
    {
        return Cache::store($this->store)->get($this->index . '_' . $id);
    }

    /**
     * @param $id
     * @return void
     * @throws InvalidArgumentException
     */
    public function removeCached($id)
    {
        if ($this->hasCached($id)) {
            Cache::store($this->store)->forget($this->index . '_' . $id);
        }
    }

    /**
     * @param $id
     * @return bool
     * @throws InvalidArgumentException
     */
    public function hasCached($id)
    {
        return Cache::store($this->store)->has($this->index . '_' . $id);
    }

    /**
     * @return void
     */
    public function refreshCachedQuestion()
    {
        $questions = Question::with('options')->get();
        $this->setCached('all', $questions);
    }

    /**
     * @return Collection|array
     * @throws InvalidArgumentException
     */
    public function all()
    {
        if (!$this->hasCached('all')) {
            $this->refreshCachedQuestion();
        }
        return $this->getCached('all');
    }

    /**
     * @param $data
     * @return mixed
     * @throws InvalidArgumentException
     */
    public function create($data)
    {
        $created = Question::create($data);
        $question = Question::with('options')->find($created->id);
        $this->setCached($created->id, $question);
        $this->refreshCachedQuestion();
        return $this->getCached($created->id);
    }

    /**
     * @param $id
     * @param $data
     * @return mixed
     * @throws InvalidArgumentException
     */
    public function update($id, $data)
    {
        Question::where('id', $id)->update($data);
        $question = Question::with('options')->find($id);
        $this->setCached($question->id, $question);
        $this->refreshCachedQuestion();
        return $this->getCached($question->id);
    }

    /**
     * @param $id
     * @return void
     * @throws Exception
     * @throws InvalidArgumentException
     */
    public function delete($id)
    {
        if (!Question::find($id)) {
            throw new RuntimeException('Question not found');
        }
        Question::where('id', $id)->delete();
        $this->removeCached($id);
        $this->refreshCachedQuestion();
    }

    /**
     * @param $id
     * @return mixed
     * @throws InvalidArgumentException
     */
    public function get($id)
    {
        if (!$this->hasCached($id)) {
            $question = Question::with('options')->find($id);
            if (!$question) {
                throw new RuntimeException('Question not found');
            }
            $this->setCached($id, $question);
            $this->refreshCachedQuestion();
        }
        return $this->getCached($id);
    }
}