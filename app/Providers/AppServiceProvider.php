<?php

namespace App\Providers;

use App\Interfaces\AnswerInterface;
use App\Interfaces\QuestionInterface;
use App\Interfaces\QuestionOptionInterface;
use App\Repositories\AnswerRepository;
use App\Repositories\QuestionOptionRepository;
use App\Repositories\QuestionRepository;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(QuestionInterface::class, QuestionRepository::class);
        $this->app->bind(QuestionOptionInterface::class, QuestionOptionRepository::class);
        $this->app->bind(AnswerInterface::class, AnswerRepository::class);
    }
}
