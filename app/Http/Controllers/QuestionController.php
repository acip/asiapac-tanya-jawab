<?php

namespace App\Http\Controllers;

use App\Interfaces\QuestionInterface;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Validation\ValidationException;
use Laravel\Lumen\Http\ResponseFactory;

class QuestionController extends Controller
{
    /**
     * @var QuestionInterface $question
     */
    private $question;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(QuestionInterface $question)
    {
        $this->question = $question;
    }

    /**
     * @OA\Get(
     *     path="/api/v1/question",
     *     operationId="index",
     *     tags={"Question"},
     *     @OA\Response(
     *         response="200",
     *         description="ok",
     *         content={
     *             @OA\MediaType(
     *                 mediaType="application/json",
     *                 @OA\Schema(
     *                     @OA\Property(
     *                         property="code",
     *                         type="integer",
     *                         description="The response code"
     *                     ),
     *                     @OA\Property(
     *                         property="status",
     *                         type="string",
     *                         description="The response message"
     *                     ),
     *                     @OA\Property(
     *                         property="questions",
     *                         type="array",
     *                         description="The response data",
     *                         @OA\Items
     *                     ),
     *                     example={
     *                         "code": 200,
     *                         "status": "OK",
     *                         "questions": {}
     *                     }
     *                 )
     *             )
     *         }
     *     )
     * )
     * @return Response|ResponseFactory
     */
    public function index()
    {
        $result = $this->question->all();
        return $this->responseSuccess([
            'questions' => $result
        ]);
    }

    /**
     * @OA\Get(
     *     path="/api/v1/question/{id}",
     *     operationId="show",
     *     tags={"Question"},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="The Question ID",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="ok",
     *         content={
     *             @OA\MediaType(
     *                 mediaType="application/json",
     *                 @OA\Schema(
     *                     @OA\Property(
     *                         property="code",
     *                         type="integer",
     *                         description="The response code"
     *                     ),
     *                     @OA\Property(
     *                         property="status",
     *                         type="string",
     *                         description="The response message"
     *                     ),
     *                     @OA\Property(
     *                         property="question",
     *                         type="array",
     *                         description="The response data",
     *                         @OA\Items
     *                     ),
     *                     example={
     *                         "code": 200,
     *                         "status": "OK",
     *                         "question": {}
     *                     }
     *                 )
     *             )
     *         }
     *     )
     * )
     * @param $id
     * @return Response|ResponseFactory
     */
    public function show($id)
    {
        try {
            return $this->responseSuccess([
                'question' => $this->question->get($id)
            ]);
        } catch (\Throwable $t) {
            return $this->responseError($t->getMessage());
        }
    }

    /**
     * @OA\Post(
     *     path="/api/v1/question",
     *     operationId="create",
     *     tags={"Question"},
     *     @OA\RequestBody(
     *        required=true,
     *        description="Pass question detail",
     *        @OA\JsonContent(
     *           required={"text"},
     *           @OA\Property(property="text", type="string", example="Test")
     *        ),
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="Returns inserted question"
     *     ),
     *     @OA\Response(
     *         response="400",
     *         description="Error",
     *     ),
     * )
     * @param Request $request
     * @return Response|ResponseFactory
     * @throws ValidationException
     */
    public function create(Request $request)
    {
        $params = $this->validate($request, [
            'text' => 'required|string',
        ]);
        $result = $this->question->create($params);
        if ($result) {
            return $this->responseSuccess([
                'question' => $result
            ]);
        }
        return $this->responseError();
    }

    /**
     * @OA\Put (
     *     path="/api/v1/question/{id}",
     *     operationId="udpate",
     *     tags={"Question"},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="The Question ID",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\RequestBody(
     *        required=false,
     *        description="Pass question detail",
     *        @OA\JsonContent(
     *           @OA\Property(property="text", type="string", example="Test")
     *        ),
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="Returns updated question"
     *     ),
     *     @OA\Response(
     *         response="400",
     *         description="Error",
     *     ),
     * )
     * @param $id
     * @param Request $request
     * @return Response|ResponseFactory
     * @throws ValidationException
     */
    public function update($id, Request $request)
    {
        $params = $this->validate($request, [
            'text' => 'string',
        ]);

        try {
            $result = $this->question->update($id, $params);
            return $this->responseSuccess([
                'question' => $result
            ]);
        } catch (Exception $exception) {
            return $this->responseError($exception->getMessage());
        }
    }

    /**
     * @OA\Delete(
     *     path="/api/v1/question/{id}",
     *     operationId="delete",
     *     tags={"Question"},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="The Question ID",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="ok",
     *         content={
     *             @OA\MediaType(
     *                 mediaType="application/json",
     *                 @OA\Schema(
     *                     @OA\Property(
     *                         property="code",
     *                         type="integer",
     *                         description="The response code"
     *                     ),
     *                     @OA\Property(
     *                         property="status",
     *                         type="string",
     *                         description="The response message"
     *                     ),
     *                     example={
     *                         "code": 200,
     *                         "status": "OK"
     *                     }
     *                 )
     *             )
     *         }
     *     ),
     *     @OA\Response(
     *         response="400",
     *         description="Error",
     *         content={
     *             @OA\MediaType(
     *                 mediaType="application/json",
     *                 @OA\Schema(
     *                     @OA\Property(
     *                         property="code",
     *                         type="integer",
     *                         description="The response code"
     *                     ),
     *                     @OA\Property(
     *                         property="status",
     *                         type="string",
     *                         description="The response status"
     *                     ),
     *                     @OA\Property(
     *                         property="message",
     *                         type="string",
     *                         description="The response message",
     *                     ),
     *                     example={
     *                         "code": 400,
     *                         "status": "ERROR",
     *                         "message": "Not Found"
     *                     }
     *                 )
     *             )
     *         }
     *     )
     * )
     * @param $id
     * @return Response|ResponseFactory
     */
    public function delete($id)
    {
        try {
            $this->question->delete($id);
            return $this->responseSuccess([
                'message' => 'Question has been deleted'
            ]);
        } catch (Exception $exception) {
            return $this->responseError($exception->getMessage());
        }
    }

}
