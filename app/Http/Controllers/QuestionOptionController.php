<?php

namespace App\Http\Controllers;

use App\Interfaces\QuestionOptionInterface;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Validation\ValidationException;
use Laravel\Lumen\Http\ResponseFactory;

class QuestionOptionController extends Controller
{
    /**
     * @var QuestionOptionInterface $question
     */
    private $questionOption;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(QuestionOptionInterface $questionOption)
    {
        $this->questionOption = $questionOption;
    }

    /**
     * @OA\Get(
     *     path="/api/v1/question-option/{id}",
     *     operationId="show",
     *     tags={"Question Option"},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="The Question Option ID",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="ok",
     *         content={
     *             @OA\MediaType(
     *                 mediaType="application/json",
     *                 @OA\Schema(
     *                     @OA\Property(
     *                         property="code",
     *                         type="integer",
     *                         description="The response code"
     *                     ),
     *                     @OA\Property(
     *                         property="status",
     *                         type="string",
     *                         description="The response message"
     *                     ),
     *                     @OA\Property(
     *                         property="question_option",
     *                         type="array",
     *                         description="The response data",
     *                         @OA\Items
     *                     ),
     *                     example={
     *                         "code": 200,
     *                         "status": "OK",
     *                         "question_option": {}
     *                     }
     *                 )
     *             )
     *         }
     *     )
     * )
     * @param $id
     * @return Response|ResponseFactory
     */
    public function show($id)
    {
        return $this->responseSuccess([
            'question_option' => $this->questionOption->get($id)
        ]);
    }

    /**
     * @OA\Post(
     *     path="/api/v1/question-option",
     *     operationId="create",
     *     tags={"Question Option"},
     *     @OA\RequestBody(
     *        required=true,
     *        description="Pass question detail",
     *        @OA\JsonContent(
     *           required={"question_id", "text"},
     *           @OA\Property(property="question_id", type="integer", example="1"),
     *           @OA\Property(property="text", type="string", example="Test"),
     *           @OA\Property(property="is_key", type="boolean", example="false")
     *        ),
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="Returns inserted question option"
     *     ),
     *     @OA\Response(
     *         response="400",
     *         description="Error",
     *     ),
     * )
     * @param Request $request
     * @return Response|ResponseFactory
     * @throws ValidationException
     */
    public function create(Request $request)
    {
        $params = $this->validate($request, [
            'question_id' => 'required|int',
            'text' => 'required|string',
            'is_key' => 'boolean'
        ]);

        try {
            $result = $this->questionOption->create($params);
            if ($result) {
                return $this->responseSuccess([
                    'question_option' => $result
                ]);
            }
        } catch (\Throwable $th) {
            return $this->responseError($th->getMessage());
        }

        return $this->responseError();
    }

    /**
     * @OA\Put (
     *     path="/api/v1/question-option/{id}",
     *     operationId="udpate",
     *     tags={"Question Option"},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="The Question Option ID",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\RequestBody(
     *        required=false,
     *        description="Pass question option detail",
     *        @OA\JsonContent(
     *           @OA\Property(property="question_id", type="integer", example="1"),
     *           @OA\Property(property="text", type="string", example="Test"),
     *           @OA\Property(property="is_key", type="boolean", example="false")
     *        ),
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="Returns updated question option"
     *     ),
     *     @OA\Response(
     *         response="400",
     *         description="Error",
     *     ),
     * )
     * @param $id
     * @param Request $request
     * @return Response|ResponseFactory
     * @throws ValidationException
     */
    public function update($id, Request $request)
    {
        $params = $this->validate($request, [
            'question_id' => 'int',
            'text' => 'string',
            'is_key' => 'boolean'
        ]);

        try {
            return $this->responseSuccess([
                'question_option' => $this->questionOption->update($id, $params)
            ]);
        } catch (Exception $exception) {
            return $this->responseError($exception->getMessage());
        }
    }

    /**
     * @OA\Delete(
     *     path="/api/v1/question-option/{id}",
     *     operationId="delete",
     *     tags={"Question Option"},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="The Question Option ID",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="ok",
     *         content={
     *             @OA\MediaType(
     *                 mediaType="application/json",
     *                 @OA\Schema(
     *                     @OA\Property(
     *                         property="code",
     *                         type="integer",
     *                         description="The response code"
     *                     ),
     *                     @OA\Property(
     *                         property="status",
     *                         type="string",
     *                         description="The response message"
     *                     ),
     *                     example={
     *                         "code": 200,
     *                         "status": "OK"
     *                     }
     *                 )
     *             )
     *         }
     *     ),
     *     @OA\Response(
     *         response="400",
     *         description="Error",
     *         content={
     *             @OA\MediaType(
     *                 mediaType="application/json",
     *                 @OA\Schema(
     *                     @OA\Property(
     *                         property="code",
     *                         type="integer",
     *                         description="The response code"
     *                     ),
     *                     @OA\Property(
     *                         property="status",
     *                         type="string",
     *                         description="The response status"
     *                     ),
     *                     @OA\Property(
     *                         property="message",
     *                         type="string",
     *                         description="The response message",
     *                     ),
     *                     example={
     *                         "code": 400,
     *                         "status": "ERROR",
     *                         "message": "Not Found"
     *                     }
     *                 )
     *             )
     *         }
     *     )
     * )
     * @param $id
     * @return Response|ResponseFactory
     */
    public function delete($id)
    {
        try {
            $this->questionOption->delete($id);
            return $this->responseSuccess([
                'message' => 'Question option has been deleted'
            ]);
        } catch (Exception $exception) {
            return $this->responseError($exception->getMessage());
        }
    }

}
