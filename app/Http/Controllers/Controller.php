<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;

/**
 * @OA\Info(
 *   title="Example API",
 *   version="1.0",
 *   @OA\Contact(
 *     email="support@example.com",
 *     name="Support Team"
 *   )
 * )
 */

class Controller extends BaseController
{
    protected function responseSuccess($data = [], $code = 200, $status = 'OK')
    {
        $response = ['code' => $code, 'status' => $status];
        if ($data) {
            $response = array_merge($response, $data);
        }
        return response($response);
    }

    protected function responseError($message = 'Something Wrong', $code = 400, $status = 'ERROR')
    {
        return response([
            'code' => $code,
            'status' => $status,
            'message' => $message
        ], 400);
    }
}
